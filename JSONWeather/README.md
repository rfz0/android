Simple example of fetching weather data from openweathermap.org using
JSON.  
![screenshot](screenshot.png)  
[AsyncTaskListener.java](src/com/example/jsonweather/AsyncTaskListener.java)
AsyncTask listener interface.  
[DateHelper.java](src/com/example/jsonweather/DateHelper.java)
Date helper.  
[DetailsListFragment.java](src/com/example/jsonweather/DetailsListFragment.java)
List fragment for weather details.  
[JsonHelper.java](src/com/example/jsonweather/JsonHelper.java)
JSON helper.  
[ListFragment.java](src/com/example/jsonweather/ListFragment.java)
Generic list fragment.  
[MainActivity.java](src/com/example/jsonweather/MainActivity.java)
Main activity.  
[SearchDialogFragment.java](src/com/example/jsonweather/SearchDialogFragment.java)
Dialog for selecting location source.  
[TextFragment.java](src/com/example/jsonweather/TextFragment.java)
TextView fragment.  
[TextInputDialogFragment.java](src/com/example/jsonweather/TextInputDialogFragment.java)
Dialog for entering text.  
[WeatherUtils.java](src/com/example/jsonweather/WeatherUtils.java)
Weather helper.  
[activity_main.xml](res/layout/activity_main.xml)
Main activity layout.  
[fragment_title.xml](res/layout/fragment_title.xml)
Title fragment layout.  
[fragment_weather_list.xml](res/layout/fragment_weather_list.xml)
Weather details list fragment layout.  
[list_item.xml](res/layout/list_item.xml)
List item layout.  
[res/menu/actions.xml](res/menu/actions.xml)
Actionbar actions layout.  
