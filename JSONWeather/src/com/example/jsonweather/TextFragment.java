package com.example.jsonweather;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

public class TextFragment extends SherlockFragment{
	
	private TextView mTextView;
	private String mText;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			mText = savedInstanceState.getString("text");
		}
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_title, container, false);
		mTextView = (TextView) view.findViewById(R.id.text);
		if (mText != null) {
			mTextView.setText(savedInstanceState.getCharSequence("text"));
		}
		return view;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putCharSequence("text", mTextView.getText());
	}
	
	public void setText(CharSequence text) {
		mTextView.setText(text);
	}

	public static TextFragment newInstance() {
		return new TextFragment();
	}
}
