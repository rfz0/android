package com.example.jsonweather;

public class WeatherUtils {
	public static final char DEGREE_SYMBOL = '\u00B0';
	
	public static Double CelsiusToFahrenheit(Double C) {
		return C * 9 / 5 + 32;
	}
	public static Double KelvinToFahrenheit(Double K) {
		return (K - 273.15) * 9 / 5 + 32;
	}
	public static Double FahrenheitToCelsius(Double F) {
		return (5.0 / 9) * (F - 32);
	}
	public static Double CelsiusToKelvin(Double C) {
		return C + 273.15;
	}
	public static Double KelvinToCelsius(Double K) {
		return K - 273.15;
	}
	public static Double FahrenheitToKelvin(Double F) {
		return (F - 32) * 5 / 9 + 273.15;
	}
}
