package com.example.jsonweather;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.SherlockDialogFragment;

public class TextInputDialogFragment extends SherlockDialogFragment {
	
	public interface TextInputDialogListener {
		public void onTextInput(String text);
	}
	
	public static TextInputDialogFragment newInstance(String title, String message, String hint) {
		TextInputDialogFragment f = new TextInputDialogFragment();
		Bundle args = new Bundle();
		args.putString("title", title);
		args.putString("message", message);
		args.putString("hint", hint);
		f.setArguments(args);
		return f;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Bundle args = getArguments();
		Activity activity = getActivity();
		final EditText et = new EditText(activity);
		et.setHint(args.getString("hint"));
		et.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				dismiss();
				onSelect(et.getText().toString());
				return true;
			}
		});
		return new AlertDialog.Builder(activity)
		.setTitle(args.getString("title"))
		.setMessage(args.getString("message"))
		.setView(et)
		.setNegativeButton("Cancel", null)
		.setPositiveButton("Select", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				onSelect(et.getText().toString());
			}
		}).create();
	}
	
	private void onSelect(String text) {
		((TextInputDialogListener)getActivity()).onTextInput(text);
	}
}
