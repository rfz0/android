package com.example.jsonweather;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockDialogFragment;

public class SearchDialogFragment extends SherlockDialogFragment {
	
	public interface SearchDialogListener {
		public void onSearchSelect(int which);
	}
	
	public static SearchDialogFragment newInstance() {
		return new SearchDialogFragment();
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		return new AlertDialog.Builder(getActivity())
		.setTitle("Search")
		.setItems(new String[] {
					"Automatically Determine Location",
					"Manually Enter Location"
				}, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				((SearchDialogListener)getActivity()).onSearchSelect(which);
			}
		})
		.create();
	}
}
