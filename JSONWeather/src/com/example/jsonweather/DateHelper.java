package com.example.jsonweather;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
	public static String formatDate(String format, long seconds) {
		return new SimpleDateFormat(format, Locale.getDefault()).format(new Date(seconds * 1000));
	}
}
