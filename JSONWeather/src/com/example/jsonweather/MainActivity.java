package com.example.jsonweather;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.example.jsonweather.SearchDialogFragment.SearchDialogListener;
import com.example.jsonweather.TextInputDialogFragment.TextInputDialogListener;

public class MainActivity extends SherlockFragmentActivity implements
		DetailsListFragment.UpdateListener, SearchDialogListener, TextInputDialogListener {

	private static final String DEBUG_TAG = MainActivity.class.getName();
	private static final String URL = "http://m.openweathermap.org/";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ActionBar ab = getSupportActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		ab.setDisplayShowTitleEnabled(false);
		ab.setIcon(R.drawable.launcher);
		if (savedInstanceState == null) {
			TextFragment f1 = TextFragment.newInstance();
			TextFragment f2 = TextFragment.newInstance();
			DetailsListFragment f3 = DetailsListFragment
					.newInstance();
			getSupportFragmentManager().beginTransaction()
					.add(R.id.frame1, f1, "f1")
					.add(R.id.frame2, f2, "f2")
					.add(R.id.frame3, f3, "f3")
					.hide(f1)
					.hide(f2)
					.hide(f3)
					.commit();
			SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
			String loc = prefs.getString("location", null);
			if (loc != null) {
				f3.setLocation(loc);
				f3.refresh();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getSupportMenuInflater().inflate(R.menu.actions, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.website:
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URL));
			startActivity(intent);
			return true;
		case R.id.refresh:
			DetailsListFragment f = (DetailsListFragment) getSupportFragmentManager()
					.findFragmentByTag("f3");
			f.refresh();
			return true;
		case R.id.search:
			SearchDialogFragment.newInstance().show(getSupportFragmentManager(), "search_dialog");
			return true;
		default:
			return false;
		}
	}

	@Override
	public void onUpdate(String location, String weather) {
		FragmentManager fm = getSupportFragmentManager();
		TextFragment f = (TextFragment) fm.findFragmentByTag("f1");
		f.setText(location);
		fm.beginTransaction()
				.setCustomAnimations(android.R.anim.slide_in_left,
						android.R.anim.slide_out_right).show(f).commit();
		f = (TextFragment) fm.findFragmentByTag("f2");
		f.setText(weather);
		fm.beginTransaction()
				.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
				.show(f).commit();
		fm.beginTransaction()
				.setCustomAnimations(android.R.anim.slide_in_left,
						android.R.anim.slide_out_right)
				.show(fm.findFragmentByTag("f3")).commit();
		SharedPreferences prefs = getSharedPreferences("preferences", MODE_PRIVATE);
		Editor ed = prefs.edit();
		ed.putString("location", location);
		ed.commit();
	}

	@Override
	public void onSearchSelect(int which) {
		switch (which) {
		case 0:
			Location location = getLocation();
			if (location != null) {
				FragmentManager fm = getSupportFragmentManager();
				DetailsListFragment f = (DetailsListFragment) fm.findFragmentByTag("f3");
				f.setLatitudeLongitude(location.getLatitude(), location.getLongitude());
				f.refresh();
			} else {
				Toast.makeText(this, "Location not available", Toast.LENGTH_SHORT).show();
			}
			break;
		case 1:
			TextInputDialogFragment.newInstance("Select Location", "Enter City", "e.g., Los Angeles")
			.show(getSupportFragmentManager(), "input_dialog");
			break;
		}
	}

	private Location getLocation() {
		LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		String bestProvider = lm.getBestProvider(criteria, false);
		if (bestProvider == null) {
			Toast.makeText(this, "No provider available", Toast.LENGTH_SHORT).show();
			return null;
		} else {
			// TODO background location updates
		}
		List<String> providers = lm.getAllProviders();
		Location bestLocation = null;
		float bestAccuracy = Float.MAX_VALUE;
		long bestTime = 0;
		long minTime = 0;
		for (String provider : providers) {
			Location location = lm.getLastKnownLocation(provider);
			if (location != null) {
				float accuracy = location.getAccuracy();
				long time = location.getTime();
				if ((time > minTime && accuracy < bestAccuracy)) {
					bestLocation = location;
					bestAccuracy = accuracy;
					bestTime = time;
				} else if (time < minTime && bestAccuracy == Float.MAX_VALUE && time > bestTime) {
					bestLocation = location;
					bestTime = time;
				}
			}
		}
		return bestLocation;
	}

	@Override
	public void onTextInput(String location) {
		FragmentManager fm = getSupportFragmentManager();
		DetailsListFragment f = (DetailsListFragment)fm.findFragmentByTag("f3");
		f.setLocation(location);
		f.refresh();
	}
}
