package com.example.jsonweather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

public class JsonHelper {

	private static final String TAG = JsonHelper.class.getName();

	public static String readJSONFromURL(String url) throws IOException {
		String json = null;
		Log.d(TAG, url);
		HttpURLConnection c = (HttpURLConnection) new URL(url).openConnection();
		try {
			Log.d(TAG, "response code " + c.getResponseCode() + " " + c.getResponseMessage());
			if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						c.getInputStream()));
				StringBuilder sb = new StringBuilder();
				try {
					String s;
					while ((s = br.readLine()) != null) {
						sb.append(s);
					}
				} finally {
					br.close();
				}
				json = sb.toString();
				Log.d(TAG, String.valueOf(json));
			} else {
				// TODO
			}
		} finally {
			c.disconnect();
		}
		return json;
	}

	public static <T extends Object> T getObjectFromJSONString(String json,
			Class<T> cls) {
		return (json == null) ? null : new Gson().fromJson(new JsonParser()
				.parse(json).getAsJsonObject(), cls);
	}

	public static <T extends Object> T getObjectFromURL(String url, Class<T> cls) {
		String json = null;
		try {
			json = readJSONFromURL(url);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return getObjectFromJSONString(json, cls);
	}

	public static class DownloadJsonTask extends
			AsyncTask<String, Void, String> {

		private AsyncTaskListener<Void, String> mListener;

		public DownloadJsonTask(AsyncTaskListener<Void, String> listener) {
			mListener = listener;
		}

		@Override
		protected String doInBackground(String... params) {
			String s = null;
			try {
				s = readJSONFromURL(params[0]);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return s;
		}

		@Override
		protected void onPostExecute(String result) {
			mListener.onPostExecute(result);
		}

	}
}
