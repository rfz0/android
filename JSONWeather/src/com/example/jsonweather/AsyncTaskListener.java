package com.example.jsonweather;

public interface AsyncTaskListener<T1, T2> {
	public void onPublishProgress(T1 t);
	public void onPostExecute(T2 t);
}
