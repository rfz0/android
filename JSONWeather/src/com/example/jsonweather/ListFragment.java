package com.example.jsonweather;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;

public class ListFragment extends SherlockListFragment {
	
	private ArrayList<ListItem> mList;
	private ListAdapter mAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		mList = new ArrayList<ListItem>();
		mAdapter = new ListAdapter(getActivity(), R.layout.list_item, mList);
		setListAdapter(mAdapter);
		super.onCreate(savedInstanceState);
	}
	
	public void clearAdapter() {
		if (mAdapter != null)
			mAdapter.clear();
	}
	
	static class ListItem {
		
		private String mName;
		private String mValue;
		
		public ListItem(String name, String value) {
			setName(name);
			setValue(value);
		}

		private void setName(String name) {
			mName = name;
		}
		
		private void setValue(String value) {
			mValue = value;
		}

		public CharSequence getName() {
			return mName;
		}

		public CharSequence getValue() {
			return mValue;
		}
	}
	
	static class ListAdapter extends ArrayAdapter<ListItem> {

		private int mTextViewResourceId;
		
		public ListAdapter(Context context, int textViewResourceId,
				List<ListItem> objects) {
			super(context, textViewResourceId, objects);
			mTextViewResourceId = textViewResourceId;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout view;
			if (convertView == null) {
				view = new LinearLayout(getContext());
				LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = (LinearLayout) inflater.inflate(mTextViewResourceId, view, true);
			} else {
				view = (LinearLayout) convertView;
			}
			
			ListItem item = getItem(position);
			
			((TextView)view.findViewById(R.id.textView1)).setText(item.getName());
			((TextView)view.findViewById(R.id.textView2)).setText(item.getValue());

			return view;
		}
		
	}
}
