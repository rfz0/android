package com.example.jsonweather;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class DetailsListFragment extends ListFragment implements
		AsyncTaskListener<Void, String> {

	public interface UpdateListener {
		public void onUpdate(String location, String weather);
	}

	private static final String URL = "http://api.openweathermap.org/data/2.5/weather?";
	private static final String QUERY_URL = URL + "q=%s";
	private static final String COORD_URL = URL + "lat=%f&lon=%f";
	private static final String TAG = DetailsListFragment.class.getName();
	private boolean mInitialize = true;
	private String mLocation;
	private double mLatitude;
	private double mLongitude;
	private boolean mUseCoordinates;

	public static DetailsListFragment newInstance() {
		return new DetailsListFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setRetainInstance(true);
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_weather_list, container,
				false);
		if (mInitialize == true) {
			//refresh();
			mInitialize = false;
		}
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	public String getLocation() {
		return mLocation;
	}

	public void setLocation(String location) {
		mLocation = location;
		mUseCoordinates = false;
	}
	
	public double getLatitude() {
		return mLatitude;
	}
	
	public double getLongitude() {
		return mLongitude;
	}
	
	public void setLatitudeLongitude(double latitude, double longitude) {
		mLatitude = latitude;
		mLongitude = longitude;
		mUseCoordinates = true;
	}

	public void refresh() {
		if (mLocation == null && mLatitude == Double.valueOf(0)
				&& mLongitude == Double.valueOf(0))
			return;
		clearAdapter();
		try {
			new JsonHelper.DownloadJsonTask(this).execute(
					mUseCoordinates ? String.format(COORD_URL, getLatitude(), getLongitude()) :
					String.format(QUERY_URL, URLEncoder.encode(getLocation(), "UTF-8")));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onPublishProgress(Void t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPostExecute(String json) {
		try {
			if (new JSONObject(json).get("cod").equals("404")) {
				Toast.makeText(getActivity(), "City not found", Toast.LENGTH_LONG).show();
				return;
			}
		} catch (JSONException e) {}
		Data data = JsonHelper.getObjectFromJSONString(json, Data.class);
		if (data == null)
			return;
		ListAdapter a = (ListAdapter) getListAdapter();
		a.add(new ListItem("Date", DateHelper.formatDate(
				"yyyy-MM-dd  hh:mm:ss aaa z", data.dt.longValue())));
		if (data.sys != null) {
			a.add(new ListItem("Sunrise", DateHelper.formatDate("hh:mm aaa z",
					data.sys.sunrise.longValue())));
			a.add(new ListItem("Sunset", DateHelper.formatDate("hh:mm aaa z",
					data.sys.sunset.longValue())));
		}
		if (data.coord != null) {
			a.add(new ListItem("Latitude", Double.valueOf(data.coord.lat)
					.toString()));
			a.add(new ListItem("Longitude", Double.valueOf(data.coord.lon)
					.toString()));
		}
		if (data.clouds != null) {
			a.add(new ListItem("Cloudiness", Double.valueOf(data.clouds.all)
					.toString() + '%'));
		}
		if (data.wind != null) {
			a.add(new ListItem("Wind Speed", Double.valueOf(data.wind.speed)
					.toString()
					+ "mps@"
					+ Double.valueOf(data.wind.deg).toString()
					+ WeatherUtils.DEGREE_SYMBOL));
		}
		if (data.main != null) {
			a.add(new ListItem("Humidity", Double.valueOf(data.main.humidity)
					.toString() + '%'));
			a.add(new ListItem("Pressure", Double.valueOf(data.main.pressure)
					.toString() + "hPa"));
			a.add(new ListItem("Temperature", String.format("%.2f",
					WeatherUtils.KelvinToFahrenheit(data.main.temp))
					+ WeatherUtils.DEGREE_SYMBOL + 'F'));
			a.add(new ListItem("Minimum", String.format("%.2f",
					WeatherUtils.KelvinToFahrenheit(data.main.temp_min))
					.toString()
					+ WeatherUtils.DEGREE_SYMBOL + 'F'));
			a.add(new ListItem("Maximum", String.format("%.2f",
					WeatherUtils.KelvinToFahrenheit(data.main.temp_max))
					.toString()
					+ WeatherUtils.DEGREE_SYMBOL + 'F'));
		}
		a.notifyDataSetChanged();
		if (data.sys != null && data.weather != null) {
			((UpdateListener) getActivity()).onUpdate(data.name + ", "
					+ data.sys.country, data.weather.get(0).description);
		}
	}

	public static class Data {
		public Coord coord;
		public Sys sys;
		public List<Weather> weather;
		public String base;
		public Main main;
		public Wind wind;
		public Clouds clouds;
		public Double dt;
		public Double id;
		public String name;
		public String cod;

		static class Coord {
			public Double lon;
			public Double lat;
		}

		static class Sys {
			public String country;
			public Double sunrise;
			public Double sunset;
		}

		static class Weather {
			public Double id;
			public String main;
			public String description;
			public String icon;
		}

		static class Main {
			public Double temp;
			public Double humidity;
			public Double pressure;
			public Double temp_min;
			public Double temp_max;
		}

		static class Wind {
			public Double speed;
			public Double gust;
			public Double deg;
		}

		static class Clouds {
			public Double all;
		}
	}
}
