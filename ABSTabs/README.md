Tabs using [ActionBarSherlock](http://actionbarsherlock.com/) which supports Android 2.1 to 4.2.  

[MainActivity.java](src/com/example/abstabs/MainActivity.java)
Main activity.  
[MyFragment.java](src/com/example/abstabs/MyFragment.java)
TextView fragment.  
[MyListFragment.java](src/com/example/abstabs/MyListFragment.java)
ListView Fragment.  
[ListItems.java](src/com/example/abstabs/ListItems.java)
ListView item strings.  
[activity\_main.xml](res/layout/activity_main.xml)
Main activity layout.  
[fragment.xml](res/layout/fragment.xml)
TextView fragment layout.  
[list\_fragment.xml](res/layout/list_fragment.xml)
ListView fragment layout.  
