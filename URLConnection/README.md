Simple example of fetching data over HTTP.

Shows progress indicator and runs the download in a separate task.

[DownloadTask.java](src/com/example/urlconnection/DownloadTask.java)
AsyncTask for fetching HTTP resource.  
[EditTextFragment.java](src/com/example/urlconnection/EditTextFragment.java)
EditText fragment containing EditText and Button for submitting a URL.  
[MainActivity.java](src/com/example/urlconnection/MainActivity.java) Main activity.  
[ProgressUpdateListener.java](src/com/example/urlconnection/ProgressUpdateListener.java)
Interface for listener to receive published progress.  
[AsyncTaskCompleteListener.java](src/com/example/urlconnection/AsyncTaskCompleteListener.java)
Interface for listener to receive result from AsyncTask.  
[activity\_main.xml](res/layout/activity_main.xml)
Layout for main activity.  
[edit\_text\_fragment.xml](res/layout/edit_text_fragment.xml)
Layout for edit text and button fragment.
