package com.example.urlconnection;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity 
	implements EditTextFragment.OnSubmitListener {
	
	private DownloadTask task = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);
		Button button = (Button)findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (task != null)
					task.cancel(true);
					setProgressBarIndeterminateVisibility(false);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onSubmit(String string) {
		download(string);
	}

	private void download(String url) {
		setProgressBarIndeterminateVisibility(true);
		final TextView tv = (TextView)findViewById(R.id.textView1);
		tv.setText("loading " + url);
		task = new DownloadTask(new AsyncTaskCompleteListener<String>() {
			@Override
			public void onCompleteTask(String result) {
				tv.setText(result);
				setProgressBarIndeterminateVisibility(false);
			}
		},
		new ProgressUpdateListener<Long>() {
			@Override
			public void onProgressUpdate(Long value) {
				tv.setText("read " + String.valueOf(value));
			}
		});
		task.execute(url);
	}

}
