package com.example.urlconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;
import android.util.Log;

public class DownloadTask extends AsyncTask<String, Long, String> {

	private static final String TAG = "DownloadTask";
	private AsyncTaskCompleteListener<String> listener;
	private ProgressUpdateListener<Long> progressListener;
	
	public DownloadTask(AsyncTaskCompleteListener<String> listener,
			ProgressUpdateListener<Long> progressListener) {
		this.listener = listener;
		this.progressListener = progressListener;
	}
	
	protected String doInBackground(String...urls) {
		String result = null;
		try {
			URL url = new URL(urls[0]);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConnection = (HttpURLConnection)connection;
			httpConnection.setConnectTimeout(5000);
			int responseCode = httpConnection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				InputStream in = httpConnection.getInputStream();
				result = readStream(in);
			}
		} catch (MalformedURLException e) {
			Log.d(TAG, "Malformed URL exception " + e.getLocalizedMessage());
		} catch (SocketTimeoutException e) {
			Log.d(TAG, "Socket timeout exception" + e.getMessage());
		} catch (IOException e) {
			Log.d(TAG, "IO exception " + e.getMessage());
		}
		return result;
	}
	
	private String readStream(InputStream in) {
		InputStreamReader is = new InputStreamReader(in);
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(is);
		try {
			long size = 0;
			String str;
			while ((str = br.readLine()) != null) {
				sb.append(str + "\n");
				size += str.length();
				publishProgress(size);
			}
		} catch (IOException e) {
			Log.d(TAG, "IO exception" + e.getMessage());
		}
		try {
			br.close();
			is.close();
			in.close();
		} catch (IOException e){}
		return sb.toString();
	}
	
	@Override
	protected void onProgressUpdate(Long... values) {
		super.onProgressUpdate(values);
		progressListener.onProgressUpdate(values[0]);
	}
	
	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		listener.onCompleteTask(result);
	}
	
}
