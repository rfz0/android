package com.example.urlconnection;

public interface AsyncTaskCompleteListener<T> {
	public void onCompleteTask(T result);
}
