package com.example.urlconnection;

public interface ProgressUpdateListener<T> {
	public void onProgressUpdate(T value);
}
