Simple example of fetching weather data from weather.gov using
XMLPullParser.

![screenshot](screenshot.png)

[DownloadImageTask.java](src/com/example/weather/DownloadImageTask.java)
AsyncTask for downloading image.  
[EditTextFragment.java](src/com/example/weather/EditTextFragment.java)
EditText fragment containing EditText and Button for submitting a
weather station.  
[MainActivity.java](src/com/example/weather/MainActivity.java)
Main activity.  
[ImageViewFragment.java](src/com/example/weather/ImageViewFragment.java)
Fragment containing ImageView.  
[ListItem.java](src/com/example/weather/ListItem.java)
List item class.  
[ListItemAdapter.java](src/com/example/weather/ListItemAdapter.java)
Adapter for list items.  
[ListItemView.java](src/com/example/weather/ListItemView.java)
View for list items.  
[WeatherListFragment.java](src/com/example/weather/WeatherListFragment.java)
Fragment containing weather list.  
[AsyncTaskCompleteListener.java](src/com/example/weather/AsyncTaskCompleteListener.java)
Interface for listener to receive result from AsyncTask.  
[activity\_main.xml](res/layout/activity_main.xml)
Layout for main activity.  
[edit\_text\_fragment.xml](res/layout/edit_text_fragment.xml)
Layout for edit text and button fragment.  
[image\_fragment.xml](res/layout/image_fragment.xml)
Layout for ImageView fragment.  
[list\_fragment.xml](res/layout/list_fragment.xml)
Layout for WeatherListFragment  
[list\_item.xml](res/layout/list_item.xml)
Layout for ListItem
