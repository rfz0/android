package com.example.weather;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;

public class MainActivity extends Activity implements
		EditTextFragment.OnSubmitListener, WeatherListFragment.ImageUrlListener {
	protected static final String TAG = "Weather";
	private WeatherListFragment weatherListFragment;
	private ImageViewFragment imageViewFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FragmentManager fm = getFragmentManager();
		weatherListFragment = (WeatherListFragment) fm
				.findFragmentById(R.id.weatherListFragment);
		imageViewFragment = (ImageViewFragment) fm
				.findFragmentById(R.id.imageViewFragment);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onSubmit(String string) {
		weatherListFragment.onSubmit(string);
	}

	@Override
	public void setImageUrl(final String url) {
		new DownloadImageTask(new AsyncTaskCompleteListener<Bitmap>() {
			@Override
			public void onCompleteTask(Bitmap result) {
				imageViewFragment.setImageBitmap(result);
			}
		}).execute(url);
	}
}
