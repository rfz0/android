package com.example.weather;

public class ListItem {
	private String key;
	private String val;
	public ListItem(String key, String val) {
		this.key = key;
		this.val = val;
	}
	public String getKey() {
		return key;
	}
	public String getVal() {
		return val;
	}
}
