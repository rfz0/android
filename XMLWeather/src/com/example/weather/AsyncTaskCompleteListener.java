package com.example.weather;

public interface AsyncTaskCompleteListener<T> {
	public void onCompleteTask(T result);
}
