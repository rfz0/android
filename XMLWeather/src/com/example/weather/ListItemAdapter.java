package com.example.weather;

import java.util.List;

import android.content.Context;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ListItemAdapter extends ArrayAdapter<ListItem> {
	private int resource;

	public ListItemAdapter(Context context, int textViewResourceId,
			List<ListItem> objects) {
		super(context, textViewResourceId, objects);
		resource = textViewResourceId;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LinearLayout layout;
		if (convertView == null) {
			layout = new LinearLayout(getContext());
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = (LinearLayout)inflater.inflate(resource, layout, true);
		} else
			layout = (LinearLayout)convertView;
		ListItem item = getItem(position);
		TextView tv1 = (TextView)layout.findViewById(R.id.textView1);
		tv1.setText(item.getKey());
		TextView tv2 = (TextView)layout.findViewById(R.id.textView2);
		tv2.setText(item.getVal());
		Linkify.addLinks(tv2, Linkify.WEB_URLS);
		
		return layout;
	}

}
