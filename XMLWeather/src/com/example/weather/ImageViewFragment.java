package com.example.weather;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageViewFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.image_fragment, container, true);
	}

	public void setImageBitmap(Bitmap bm) {
		ImageView iv = (ImageView) getView();
		iv.setImageBitmap(bm);
	}
}
