package com.example.weather;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.os.Handler;
import android.provider.UserDictionary.Words;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class WeatherListFragment extends ListFragment {
	
	public interface ImageUrlListener {
		public void setImageUrl(String url);
	}
	
	private ListItemAdapter adapter;
	private ArrayList<ListItem> list;
	private Handler handler;
	private String station;
	private ImageUrlListener imageUrlListener;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.list_fragment, container, true);
		list = new ArrayList<ListItem>();
		adapter = new ListItemAdapter(getActivity(), R.layout.list_item, list);
		setListAdapter(adapter);
		handler = new Handler();
		station = getString(R.string.inital_station);
		new Thread(new Runnable() {
			@Override
			public void run() {
				refresh();
			}
		}).start();
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		imageUrlListener = (ImageUrlListener)activity;
	}

	protected void refresh() {
		URL url;
		try {
			url = new URL(String.format(getString(R.string.url), station));
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConnection = (HttpURLConnection) connection;
			httpConnection.connect();
			if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				InputStream is = httpConnection.getInputStream();
				processStream(is);
			}
		} catch (MalformedURLException e) {
			Log.d(MainActivity.TAG, "Malformed URL Exception", e);
		} catch (IOException e) {
			Log.d(MainActivity.TAG, "IO Exception", e);
		}
	}

	private void processStream(InputStream is) {
		XmlPullParserFactory factory;
		try {
			factory = XmlPullParserFactory.newInstance();
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(is, null);
			int eventType = xpp.getEventType();
			if (eventType == XmlPullParser.START_DOCUMENT) {
				eventType = xpp.next();
				if (eventType == XmlPullParser.START_TAG
						&& xpp.getName().equals("current_observation")) {
					String icon_url_base = null, icon_url_name = null;
					handler.post(new Runnable() {
						@Override
						public void run() {
							clear();
						}
					});
					while (!(eventType == XmlPullParser.END_TAG && xpp
							.getName().equals("current_observation"))) {
						eventType = xpp.next();
						if (eventType == XmlPullParser.START_TAG) {
							String name = xpp.getName();
							if (name.equals("title") || name.equals("image") ||
									name.equals("url") || name.equals("credit_URL")
									|| name.endsWith("_string"))
								continue;
							eventType = xpp.next();
							if (eventType == XmlPullParser.TEXT) {
								if (name.equals("icon_url_base")) {
									icon_url_base = xpp.getText();
									continue;
								} else if (name.equals("icon_url_name")) {
									icon_url_name = xpp.getText();
									continue;
								}
								final String key = name.replace("_", " ");
								final String val = xpp.getText();
								handler.post(new Runnable() {
									@Override
									public void run() {
										add(new ListItem(key, val));
									}
								});
							}
						}
					}
					if (icon_url_base != null && icon_url_name != null) {
						final String url = icon_url_base + icon_url_name;
						handler.post(new Runnable() {
							@Override
							public void run() {
								imageUrlListener.setImageUrl(url);
							}
						});
					}
				}
			}
		} catch (XmlPullParserException e) {
			Log.d(MainActivity.TAG, "XML Pull Parser Exception", e);
		} catch (IOException e) {
			Log.d(MainActivity.TAG, "IO Exception", e);
		}

	}

	protected void clear() {
		list.clear();
		adapter.notifyDataSetChanged();
	}

	protected void add(ListItem listItem) {
		list.add(listItem);
		adapter.notifyDataSetChanged();
	}
	
	public void onSubmit(String string) {
		station = string;
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				refresh();
			}
		});
		thread.start();
	}
}
