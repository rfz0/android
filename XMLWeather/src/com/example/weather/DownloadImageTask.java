package com.example.weather;

import java.io.InputStream;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	
	private AsyncTaskCompleteListener<Bitmap> listener;

	public DownloadImageTask(AsyncTaskCompleteListener<Bitmap> listener) {
		this.listener = listener;
	}

	@Override
	protected Bitmap doInBackground(String... urls) {
		String url = urls[0];
		Bitmap bm = null;
		try {
			InputStream is = new URL(url).openStream();
			bm = BitmapFactory.decodeStream(is);
		} catch (Exception e) {
			Log.d(MainActivity.TAG, e.getMessage());
		}
		return bm;
	}
	
	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);
		listener.onCompleteTask(result);
	}

}
