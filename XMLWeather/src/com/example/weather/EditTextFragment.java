package com.example.weather;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class EditTextFragment extends Fragment {
	public interface OnSubmitListener {
		public void onSubmit(String string);
	}
	private OnSubmitListener onSubmitListener;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.edit_text_fragment, container, true);
		final EditText editText = (EditText)view.findViewById(R.id.editText1);
		editText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN &&
						keyCode == KeyEvent.KEYCODE_ENTER) {
					onSubmitListener.onSubmit(editText.getText().toString());
					return true;
				}
				return false;
			}
		});
		Button button = (Button)view.findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onSubmitListener.onSubmit(editText.getText().toString());
			}
		});
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		onSubmitListener = (OnSubmitListener)activity;
	}
}
