Preferences.

[MainActivity.java](src/com/example/preferences/MainActivity.java)
Main activity. See onOptionsItemSelected().  
[MyFragmentPreferenceActivity.java](src/com/example/preferences/MyFragmentPreferenceActivity.java)
Activity for loading preference headers which point to preference fragments (Android 3.0 and greater).  
[MyPreferenceFragment.java](src/com/example/preferences/MyPreferenceFragment.java)
Preference Fragment for when using preference headers.  
[preference\_headers.xml](res/xml/preference_headers.xml)
Definition for preference headers.  
[MyPreferenceActivity.java](src/com/example/preferences/MyPreferenceActivity.java)
Activity for inflating preferences directly (Android less than 3.0).  
[preferences.xml](res/xml/preferences.xml)
Preferences definition. Loaded directly when using MyPreferenceActivity
(android less than 3.0) or by MyPreferenceFragment which is started
from MyFragmentPreferenceActivity (android greater than 3.0)  
