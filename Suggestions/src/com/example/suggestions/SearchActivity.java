package com.example.suggestions;

import android.app.SearchManager;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class SearchActivity extends FragmentActivity implements
		LoaderManager.LoaderCallbacks<Cursor> {

	protected static final String DEBUG_TAG = "SearchActivity";
	private static final String KEY_QUERY = "KEY_QUERY";
	private static final String SORT_ORDER = MyContentProvider.KEY_ITEM
			+ " COLLATE LOCALIZED ASC";
	private static final String[] PROJECTION = { MyContentProvider.KEY_ID,
			MyContentProvider.KEY_ITEM };
	private static final String SELECTION = MyContentProvider.KEY_ITEM + " LIKE \"%%%s%%\"";
	private SimpleCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);
		adapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_1, null,
				new String[] { MyContentProvider.KEY_ITEM },
				new int[] { android.R.id.text1 }, 0);
		ListView view = (ListView) findViewById(R.id.listView1);
		view.setAdapter(adapter);
		view.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Uri uri = ContentUris.withAppendedId(
						MyContentProvider.CONTENT_URI, id);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(uri);
				try {
					startActivity(intent);
				} catch (Exception e) {
					Log.d(DEBUG_TAG, e.getMessage());
					Toast.makeText(getApplicationContext(), e.getMessage(),
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		getSupportLoaderManager().initLoader(0, null, this);
		handleIntent(getIntent());
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
		String action = intent.getAction();
		String searchQuery = null;
		if (Intent.ACTION_SEARCH.equals(action)) {
			searchQuery = intent.getStringExtra(SearchManager.QUERY);
		} else if (Intent.ACTION_VIEW.equals(action)) {
			searchQuery = intent.getStringExtra(SearchManager.EXTRA_DATA_KEY);
		}
		doSearch(searchQuery);
	}

	private void doSearch(String searchQuery) {
		Bundle args = new Bundle();
		args.putString(KEY_QUERY, searchQuery);
		getSupportLoaderManager().restartLoader(0, args, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String query = "0";
		if (args != null)
			query = args.getString(KEY_QUERY);
		return new CursorLoader(this, MyContentProvider.CONTENT_URI,
				PROJECTION, String.format(SELECTION, query), null, SORT_ORDER);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		adapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		adapter.swapCursor(null);
	}

}
