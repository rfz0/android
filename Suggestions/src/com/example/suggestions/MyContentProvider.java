package com.example.suggestions;

import java.util.HashMap;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class MyContentProvider extends ContentProvider {

	private MySQLiteOpenHelper helper;
	private static final HashMap<String, String> SEARCH_PROJECTION_MAP = new HashMap<String, String>();
	private static final UriMatcher matcher = new UriMatcher(
			UriMatcher.NO_MATCH);
	private static final int MATCH_ALL = 1;
	private static final int MATCH_ROW = 2;
	private static final int MATCH_SEARCH = 3;
	private static final String AUTHORITY = "com.example.suggestions.mycontentprovider";
	private static final String MIME_TYPE_ALL = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/vnd.example." + MySQLiteOpenHelper.DB_TABLE;
	private static final String MIME_TYPE_ROW = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/vnd.example." + MySQLiteOpenHelper.DB_TABLE;
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + MySQLiteOpenHelper.DB_TABLE);
	public static final String KEY_ID = "_id";
	public static final String KEY_ITEM = "ITEM";

	static {
		SEARCH_PROJECTION_MAP.put(KEY_ID, KEY_ID + " AS " + KEY_ID);
		SEARCH_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_TEXT_1, KEY_ITEM
				+ " AS " + SearchManager.SUGGEST_COLUMN_TEXT_1);
		SEARCH_PROJECTION_MAP.put(
				SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA, KEY_ITEM
						+ " AS "
						+ SearchManager.SUGGEST_COLUMN_INTENT_EXTRA_DATA);
		SEARCH_PROJECTION_MAP.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID,
				KEY_ID + " AS " + KEY_ID);

		matcher.addURI(AUTHORITY, MySQLiteOpenHelper.DB_TABLE, MATCH_ALL);
		matcher.addURI(AUTHORITY, MySQLiteOpenHelper.DB_TABLE + "/#", MATCH_ROW);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY,
				MATCH_SEARCH);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*",
				MATCH_SEARCH);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT,
				MATCH_SEARCH);
		matcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT
				+ "/*", MATCH_SEARCH);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = helper.getWritableDatabase();
		switch (matcher.match(uri)) {
		case MATCH_ROW:
			selection = KEY_ID
					+ "="
					+ uri.getLastPathSegment()
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ")" : "");
			break;
		}
		if (selection == null)
			selection = "1";
		int count = db.delete(MySQLiteOpenHelper.DB_TABLE, selection,
				selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch (matcher.match(uri)) {
		case MATCH_ALL:
			return MIME_TYPE_ALL;
		case MATCH_ROW:
			return MIME_TYPE_ROW;
		case MATCH_SEARCH:
			return SearchManager.SUGGEST_MIME_TYPE;
		default:
			throw new IllegalArgumentException("Unsupported URI " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = helper.getWritableDatabase();
		long rowId = db.insert(MySQLiteOpenHelper.DB_TABLE, null, values);
		if (rowId > -1) {
			Uri rowUri = ContentUris.withAppendedId(CONTENT_URI, rowId);
			getContext().getContentResolver().notifyChange(rowUri, null);
			return rowUri;
		}
		return null;
	}

	@Override
	public boolean onCreate() {
		helper = new MySQLiteOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		builder.setTables(MySQLiteOpenHelper.DB_TABLE);
		switch (matcher.match(uri)) {
		case MATCH_ROW:
			builder.appendWhere(KEY_ID + "=" + uri.getLastPathSegment());
			break;
		case MATCH_SEARCH:
			builder.appendWhere(KEY_ITEM + " LIKE \"%"
					+ uri.getLastPathSegment() + "%\"");
			builder.setProjectionMap(SEARCH_PROJECTION_MAP);
			break;
		}
		SQLiteDatabase db = helper.getWritableDatabase();
		return builder.query(db, projection, selection, selectionArgs, null,
				null, sortOrder);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		switch (matcher.match(uri)) {
		case MATCH_ROW:
			selection = KEY_ID
					+ "="
					+ uri.getLastPathSegment()
					+ (!TextUtils.isEmpty(selection) ? " AND (" + selection
							+ ")" : "");
			break;
		}
		SQLiteDatabase db = helper.getWritableDatabase();
		int count = db.update(MySQLiteOpenHelper.DB_TABLE, values, selection,
				selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	private static final class MySQLiteOpenHelper extends SQLiteOpenHelper {

		private static final String DB_NAME = "mydb";
		private static final int DB_VERSION = 1;
		private static final String DB_TABLE = "table1";
		private static final String SQL_CREATE_TABLE = "CREATE TABLE "
				+ DB_TABLE + "(" + KEY_ID
				+ " integer primary key autoincrement, " + KEY_ITEM
				+ " text not null);";
		private static final String SQL_DROP_TABLE = "drop table if it exists"
				+ DB_TABLE;

		public MySQLiteOpenHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SQL_CREATE_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL(SQL_DROP_TABLE);
			onCreate(db);
		}

	}
}
