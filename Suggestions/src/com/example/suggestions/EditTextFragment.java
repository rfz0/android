package com.example.suggestions;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class EditTextFragment extends Fragment {

	public interface OnSubmitListener {
		public void onSubmit(String text);
	}

	private OnSubmitListener onSubmitListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.edittext_fragment, container,
				true);
		final EditText editText = (EditText) view.findViewById(R.id.editText1);
		editText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
				if (keyEvent.getAction() == KeyEvent.ACTION_DOWN
						&& keyCode == KeyEvent.KEYCODE_ENTER) {
					onSubmitListener.onSubmit(editText.getText().toString());
					return true;
				}
				return false;
			}
		});
		Button button = (Button) view.findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				onSubmitListener.onSubmit(editText.getText().toString());
			}
		});
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.onSubmitListener = (OnSubmitListener) activity;
	}
}
