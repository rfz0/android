package com.example.suggestions;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity implements
		EditTextFragment.OnSubmitListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.search:
			onSearchRequested();
			return true;
		}
		return false;
	}

	@Override
	public void onSubmit(String text) {
		// TODO
		ContentResolver contentResolver = getContentResolver();
		ContentValues contentValues = new ContentValues();
		contentValues.put(MyContentProvider.KEY_ITEM, text);
		contentResolver.insert(MyContentProvider.CONTENT_URI, contentValues);
	}
}
