Simple SQLite-backed ContentProvider for search suggestions (using search
bar) and list.

[MainActivity.java](src/com/example/suggestions/MainActivity.java)
Main activity.  
[activity\_main.xml](res/layout/activity_main.xml)
Main activity layout.  
[main.xml](res/menu/main.xml)
Main menu definition with additional item for search.  
[EditTextFragment.java](src/com/example/suggestions/EditTextFragment.java)
Fragment for submitting new list item.  
[edittext\_fragment.xml](res/layout/edittext_fragment.xml)
Layout for fragment for submitting new list item.  
[MyContentProvider.java](src/com/example/suggestions/MyContentProvider.java)
Content provider used for both the list fragment and search suggestions.  
[SearchActivity.java](src/com/example/suggestions/SearchActivity.java)
Activity for searching.  
[search.xml](res/layout/search.xml)
Search results layout.  
[searchable.xml](res/xml/searchable.xml)
Search configuration.  
[AndroidManifest.xml](AndroidManifest.xml)
Manifest.  
